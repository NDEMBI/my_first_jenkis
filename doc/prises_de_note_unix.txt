________________Comment installer Jenkins sur unix/Ubuntu____________________

Pour changer l'utilisateur root dans bash ubuntu windows:

wsl --user root passwd <nom de l'utilisateur>


Installation de Jenkins(https://www.jenkins.io/doc/book/installing/#debianubuntu?searchId=7RPURY3FS)

1- Vérifier Java ( si Java est absent : sudo apt-get install default-jre )

2- Install jenkins

wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > \
    /etc/apt/sources.list.d/jenkins.list'

sudo apt-get update
sudo apt-get install jenkins

3- verifier le status de Jenkins

sudo service jenkins
sudo service jenkins status

4- Récuperer l'adresse ip du serveur (ifconfig)
il s'agit de inet adr:10.0.2.15
nb: ajouter le port 8080 à la fin

5- Ouvrir jenkins sur le navigateur

10.0.2.15:8080

6- Copier la clé de sécurité demandé sur la page d'accueil 

sudo less /var/lib/jenkins/secrets/initialAdminPassword

7-Copier la clé et coller dans l'interface d'installation de jenkins

8-Suivez les instructions

9- Installer les plugings proposés par Jenkins

10- :q! sortir de vim et terminé.



